# cheatman

## Overview

I discovered an incredible project the is a hangman bot that cheats. The
project is called [cheatman](https://github.com/Dan-Q/hangman-cheat). There is
a [blog post](https://danq.me/2019/09/26/cheatman/) that accompanies it and
describes the goals of the bot:

1. Legitimate: it must still be the same length, have correctly-guessed letters
	in the same places, and contain no letters that have been declared to be
	incorrect guesses.
2. Harder: after resolving the player's current guess, the number of possible
	words must be larger than the number of possible words that would have
	resulted otherwise.

With those two goals I set out to build my own cli version of cheatman.
