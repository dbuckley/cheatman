package main

import (
	"bufio"
	"fmt"
	"io"
	"math/rand"
	"os"
	"sort"
	"strings"
	"time"
)

func main() {
	in := newInput(os.Stdin)
	g := createGame(5, 5)
	g.Start()
	for {
		for g.running {
			g.printScreen()
			txt := in.prompt("Guess: ")
			g.ParseInput(txt)
		}
		g.printScreen()

		resp := in.prompt("Play again? [y/N]: ")
		if resp == "y" || resp == "Y" {
			g.Start()
		} else {
			return
		}
	}
}

type game struct {
	running bool
	history int
	log     []string

	guessed  []guess
	word     []rune
	target   string
	wordBank []string
}

func createGame(wordLen int, history int) *game {
	g := &game{
		history: history,

		log:  make([]string, wordLen),
		word: make([]rune, wordLen),
	}
	err := g.LoadWords("words.txt")
	if err != nil {
		panic(fmt.Sprintf("Failed to create game: %v", err))
	}
	return g
}

func (g *game) Start() {
	g.guessed = []guess{}
	g.running = true
	g.log = make([]string, len(g.log))
	g.word = make([]rune, len(g.word))
	g.PickWord()
	g.Log("Let the game begin!")
}

type guess struct {
	char  rune
	valid bool
}

// Prints each guess as red/green to the terminal using escape codes
func (g *guess) Print() {
	defer fmt.Printf("\x1b[0m")
	if g.valid {
		fmt.Printf("\x1b[0;32m %s", string(g.char))
		return
	}
	fmt.Printf("\x1b[1;31m %s", string(g.char))
}

func (g *game) printScreen() {
	fmt.Printf("\x1b[2J")
	fmt.Printf("\x1b[H")
	for _, l := range g.log {
		fmt.Printf(l + "\n")
	}
	fmt.Println()
	fmt.Printf("Guesses: ")
	for _, gu := range g.guessed {
		gu.Print()
	}
	fmt.Printf("\n")
	fmt.Println(g.wordScore())
	fmt.Println()
}

// Creates string with blank words as _ and seperated with a space
func (g *game) wordScore() string {
	s := strings.Builder{}
	for _, r := range g.word {
		if r == rune(0) {
			s.WriteRune('_')
		} else {
			s.WriteRune(r)
		}
		s.WriteString(" ")
	}
	return s.String()
}

func (g *game) Log(l string, args ...interface{}) {
	str := fmt.Sprintf(l, args...)
	g.log = append(g.log, str)
	g.log = g.log[1:]
}

func (g *game) ParseInput(s string) {
	if len(s) > 1 {
		g.Log("Only one letter at a time please!")
		return
	}

	s = strings.ToUpper(s)
	r := rune(s[0])

	if !((r >= 65 && r <= 90) || (r >= 97 && r <= 122)) {
		g.Log("That does not seem to be a number")
		return
	}

	for _, c := range g.guessed {
		if rune(r) == c.char {
			g.Log("You already tried %s", s)
			return
		}
	}

	valid := g.testRune(rune(r))
	gu := guess{char: rune(r), valid: valid}
	g.guessed = append(g.guessed, gu)
	g.sortGuesses()

	g.checkScore()
}

func (g *game) LoadWords(file string) error {
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	scn := bufio.NewScanner(f)
	for scn.Scan() {
		txt := scn.Text()
		if len(txt) == len(g.word) {
			g.wordBank = append(g.wordBank, txt)
		}
	}

	return nil
}

func (g *game) PickWord() {
	rand.Seed(time.Now().UnixNano())
	max := len(g.wordBank)
	ran := rand.Intn(max)
	g.target = g.wordBank[ran]
	fmt.Println(g.target)
}

func (g *game) testRune(r rune) bool {
	found := false
	for i, c := range g.target {
		if c == r {
			found = true
			g.word[i] = r
		}
	}
	return found
}

func (g *game) checkScore() {
	i := 0
	for _, r := range g.word {
		if r != rune(0) {
			i++
		}
	}

	if i == len(g.word) {
		g.running = false
		g.Log("You won! Way to go! It only took %d guesses.", len(g.guessed))
	}
}

func (g *game) sortGuesses() {
	sort.Slice(g.guessed, func(i, j int) bool { return g.guessed[i].char < g.guessed[j].char })
}

type input struct {
	reader *bufio.Reader
}

func newInput(r io.Reader) *input {
	reader := bufio.NewReader(r)
	return &input{
		reader: reader,
	}
}

func (i *input) prompt(msg string, arg ...interface{}) string {
	fmt.Printf(msg, arg...)
	r, _ := i.reader.ReadString('\n')
	return strings.Replace(r, "\n", "", -1)
}
